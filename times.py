#! /usr/bin/env python

import datetime
import json
import os
from pathlib import Path
import pprint
import simplejson
import sqlite3
import sys
import time
import traceback
import requests

API_KEY = os.environ['BRISTOL_API_KEY']


class GetUpcomingError(Exception):
    pass


def get_upcoming(stop_id: str, minutes: int = 30) -> dict:
    response = requests.get("https://bristol.api.urbanthings.io/api/2.0/rti/report",
                            params={"stopID": stop_id, "lookAheadMinutes": minutes},
                            headers={"X-Api-Key": API_KEY})

    if response.status_code != requests.codes.ok:
        raise GetUpcomingError(f"{response.status_code}: {response.text}")

    try:
        upcoming_json = response.json()
    except (json.JSONDecodeError, simplejson.errors.JSONDecodeError) as e:
        raise GetUpcomingError(f"Could not decode JSON response\n"
                               f"{e}\n"
                               f"Response: {response}\n"
                               f"Response body: {response.text}")

    try:
        return upcoming_json['data']['rtiReports'][0]['upcomingCalls']
    except TypeError as e:
        j = pprint.pformat(upcoming_json)
        raise GetUpcomingError(f"Could not get upcoming calls:\n"
                               f"{e}\n"
                               f"Response: {response}\n"
                               f"{j}")


stop_data = requests.get("https://bristol.api.urbanthings.io/api/2.0/static/transitstops",
                         params={"centerLat": 51.4556650, "centerLng": -2.5875486, "radius": 10000,
                                 "stopName": "Beaconsfield Road"},
                         headers={"X-Api-Key": API_KEY}).json()

primary_code = stop_data['data'][0]['primaryCode']

db = Path('buses.db')

new_db = not db.exists()

conn = sqlite3.connect(str(db), detect_types=sqlite3.PARSE_DECLTYPES)

if new_db:
    with conn:
        # conn.execute("DROP TABLE arrival_samples")
        conn.execute("CREATE TABLE arrival_samples (stop text, time timestamp, data json)")

while True:

    if 2 <= datetime.datetime.now().hour < 4:
        time.sleep(60)
        continue

    try:
        upcoming = get_upcoming(primary_code)
    except GetUpcomingError as e:
        print(e)
        time.sleep(1)
        continue
    except Exception as e:
        print("Unknown exception")
        traceback.print_exc()
        time.sleep(5)
        continue

    now = datetime.datetime.now(datetime.timezone.utc)

    with conn:
        conn.execute("INSERT INTO arrival_samples VALUES (?, ?, ?)",
                     (primary_code, now.isoformat(), json.dumps(upcoming)))
    print(primary_code, now)

    time.sleep(60)
