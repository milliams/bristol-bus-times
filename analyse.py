from datetime import timedelta
import sqlite3

import numpy as np
import pandas as pd

conn = sqlite3.connect('buses.db', detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)

df = pd.read_sql('SELECT * FROM arrival_samples', conn, coerce_float=False, parse_dates=['time', 'scheduled', 'expected'])

def arrived_at(row, df):
    repeats = df[df['bus_id'] == row['bus_id']]
    last = repeats.iloc[-1]
    if (df.iloc[-1]['time'] - last['time']) < timedelta(minutes=5):
        return None
    arrived = last['expected']
    return arrived

df['bus_id'] = df.apply(lambda r: hash((r['vehicle_id'], r['scheduled'])), axis=1)

df['arrived'] = df.apply(arrived_at, axis=1, df=df)

df['delay'] = df['arrived'] - df['expected']

def mean_s(s):
    return np.mean(s)


def max_s(s):
    return np.max(s)


def min_s(s):
    return np.min(s)


#print(df)

gb = df[['time', 'delay']].groupby('time')
mean_delay = gb.aggregate(mean_s)['delay'].astype('timedelta64[s]') / 60
max_delay = gb.aggregate(max_s)['delay'].astype('timedelta64[s]') / 60
min_delay = gb.aggregate(min_s)['delay'].astype('timedelta64[s]') / 60

delays = pd.DataFrame({'mean': mean_delay, 'max': max_delay, 'min': min_delay})

ax = delays.plot()
ax.set_ylabel('Time dilation (minutes)')
fig = ax.get_figure()
fig.savefig('delay.pdf')

print(delays)

